Repository with example configs different tools.

**ansible** - roles for jenkins, nexus and examples of tricky things

**aws** - templates for cloudformation

**bash** - fork with cool staff

**c++** - samples for practice 

**dotfiles** - configs for bash, vim, tmux, etc

**python** - samples for practice

**vagrant** - examples of easy vm creation

**terraform** - examples of infrastructure creation