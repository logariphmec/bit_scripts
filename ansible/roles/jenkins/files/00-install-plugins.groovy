/*
    Install required plugins and their dependencies.
*/
import jenkins.model.*
import hudson.model.*
import org.jenkinsci.plugins.*
import hudson.model.UpdateSite
import hudson.PluginWrapper

Set<String> plugins_to_install = [
    "ace-editor",
    "ansible",
    "ansicolor",
    "ant",
    "antisamy-markup-formatter",
    "apache-httpcomponents-client-4-api",
    "authentication-tokens",
    "badge",
    "bouncycastle-api",
    "branch-api",
    "build-pipeline-plugin",
    "build-time-blame",
    "build-timeout",
    "changelog-history",
    "clone-workspace-scm",
    "cloudbees-folder",
    "command-launcher",
    "conditional-buildstep",
    "configurationslicing",
    "credentials",
    "credentials-binding",
    "customized-build-message",
    "disk-usage",
    "display-url-api",
    "docker-build-step",
    "docker-commons",
    "docker-java-api",
    "docker-plugin",
    "docker-slaves",
    "docker-workflow",
    "downstream-ext",
    "durable-task",
    "email-ext",
    "envinject",
    "envinject-api",
    "environment-dashboard",
    "extended-choice-parameter",
    "extended-read-permission",
    "extensible-choice-parameter",
    "external-monitor-job",
    "external-workspace-manager",
    "flexible-publish",
    "genexus",
    "git",
    "git-client",
    "git-server",
    "github",
    "github-api",
    "github-branch-source",
    "github-pullrequest",
    "global-post-script",
    "google-login",
    "gradle",
    "groovy",
    "groovy-postbuild",
    "handlebars",
    "hipchat",
    "htmlpublisher",
    "http_request",
    "icon-shim",
    "jackson2-api",
    "javadoc",
    "jdk-tool",
    "job-dsl"
    "job-import-plugin",
    "job-restrictions",
    "jobConfigHistory",
    "jobgenerator",
    "jquery",
    "jquery-detached",
    "jsch",
    "junit",
    "ldap",
    "locale",
    "log-parser",
    "mailer",
    "mapdb-api",
    "matrix-auth",
    "matrix-project",
    "maven-artifact-choicelistprovider",
    "maven-plugin",
    "momentjs",
    "monitoring",
    "msbuild",
    "multiple-scms",
    "naginator",
    "nested-view",
    "nexus-artifact-uploader",
    "nexus-jenkins-plugin",
    "nexus-task-runner",
    "nodelabelparameter",
    "pam-auth",
    "parameterized-trigger",
    "pipeline-build-step",
    "pipeline-github-lib",
    "pipeline-graph-analysis",
    "pipeline-input-step",
    "pipeline-milestone-step",
    "pipeline-model-api",
    "pipeline-model-declarative-agent",
    "pipeline-model-definition",
    "pipeline-model-extensions",
    "pipeline-rest-api",
    "pipeline-stage-step",
    "pipeline-stage-tags-metadata",
    "pipeline-stage-view",
    "pipeline-utility-steps",
    "plain-credentials",
    "publish-over",
    "publish-over-ssh",
    "python",
    "repository-connector",
    "resource-disposer",
    "ruby-runtime",
    "run-condition",
    "run-condition-extras",
    "scm-api",
    "scm-sync-configuration",
    "script-security",
    "shelve-project-plugin",
    "sonar",
    "ssh",
    "ssh-credentials",
    "ssh-slaves",
    "structs",
    "subversion",
    "text-finder",
    "text-finder-run-condition",
    "thinBackup",
    "timestamper",
    "token-macro",
    "ui-samples-plugin",
    "view-job-filters",
    "windows-slaves",
    "workflow-aggregator",
    "workflow-api",
    "workflow-basic-steps",
    "workflow-cps",
    "workflow-cps-global-lib",
    "workflow-durable-task-step",
    "workflow-job",
    "workflow-multibranch",
    "workflow-scm-step",
    "workflow-step-api",
    "workflow-support",
    "ws-cleanup",
    "xml-job-to-job-dsl"
]

Boolean hasConfigBeenUpdated = false
UpdateSite updateSite = Jenkins.getInstance().getUpdateCenter().getById('default')
List<PluginWrapper> plugins = Jenkins.instance.pluginManager.getPlugins()

def install_plugin(shortName, UpdateSite updateSite) {
    println "Installing ${shortName} plugin."
    UpdateSite.Plugin plugin = updateSite.getPlugin(shortName)
    Throwable error = plugin.deploy(false).get().getError()
    if(error != null) {
        println "ERROR installing ${shortName}, ${error}"
    }
    null
}

// Check the update site(s) for latest plugins
println 'Checking plugin updates via Plugin Manager.'
Jenkins.instance.pluginManager.doCheckUpdatesServer()

// Any plugins need updating?
Set<String> plugins_to_update = []
plugins.each {
    if(it.hasUpdate()) {
        plugins_to_update << it.getShortName()
    }
}

if(plugins_to_update.size() > 0) {
    println "Updating plugins..."
    plugins_to_update.each {
        install_plugin(it, updateSite)
    }
    println "Done updating plugins."
    hasConfigBeenUpdated = true
}

// Get a list of installed plugins
Set<String> installed_plugins = []
plugins.each {
    installed_plugins << it.getShortName()
}

// Check to see if there are missing plugins to install
Set<String> missing_plugins = plugins_to_install - installed_plugins
if(missing_plugins.size() > 0) {
    println "Install missing plugins..."
    missing_plugins.each {
        install_plugin(it, updateSite)
    }
    println "Done installing missing plugins."
    hasConfigBeenUpdated = true
}

if(hasConfigBeenUpdated) {
    println "Saving Jenkins configuration to disk."
    Jenkins.instance.save()
    Jenkins.instance.restart()
} else {
    println "Jenkins up-to-date. Nothing to do."
}
