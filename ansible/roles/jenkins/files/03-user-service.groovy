// создает пользователя и добавляет ему ssh-ключ для доступа к системе контроля версий
public_key = 'ssh-rsa AAAAB3N....TJChv jenkins'
user = hudson.model.User.get('service')
user.setFullName('Service User')
keys = new org.jenkinsci.main.modules.cli.auth.ssh.UserPropertyImpl(public_key)
user.addProperty(keys)
user.save()
