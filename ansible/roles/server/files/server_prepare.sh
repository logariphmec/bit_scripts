#! /bin/bash
set -x 

#apt update ; apt dist-upgrade 
#apt install -y acpi aircrack-ng ansible apache2 apache2-bin apt-transport-https arp arp-scan audacious bash-completion blueman brctl bridge-utils calendar-google-provider cifs-utils cryptsetup csh curl davfs2 debian-goodies default-jdk default-jre default-jre-headless dnsutils docker-engine doublecmd-gtk e2fsprogs easy-rsa easytag exfat-utils feh ffmpeg findutils firefox free freerdp-x11 freerdp-x11-dbg gawk git git-core gnome-screenshot gnupg2 gparted hardinfo htop ipmitool iptables iptables-persistent iputils isoinfo jq keepass2 lshw luksdump lxappearance lxsession-default-apps lynx mount.cifs mplayer2 nmap nmon nslookup numlockx openconnect openjdk-8-jdk openjdk-8-jdk-headless openssl openvpn pinta pv python-pip qemu-kvm quota rclone s3cmd samba sambaclient shellcheck simplescreenrecorder smbclient sshfs sshpass sstp sstp-client sstpc strace synclient tcpdump testdisk thunderbird tmux tunctl unrar unzip vim virt-manager virtinst virtualenv vlc vpnc-scripts wget wireshark xargs xbacklight xfce4-terminal xinput zeal zenity zenmap zip fail2ban neomutt
#git htop vim curl open-vm-tools wget make net-tools openssh-server openvpn kerio git htop vim curl unzip strace xnview socat grc
#apt install -y libapache2-mod-php5  php5  php5-cli php5-common php5-json  php5-mysql  php5-readline
#apt install -y mysql-client mysql-server

echo '"\e[A": history-search-backward'  >> /etc/inputrc 
echo '"\e[B": history-search-forward'  >>  /etc/inputrc


#sed -i 's/.*Port 22/Port 25896/'  /etc/ssh/sshd_config
#sed -i 's/.*PermitRootLogin yes/PermitRootLogin no/'  /etc/ssh/sshd_config
#sed -i 's/.*PubkeyAuthenticati.*/PubkeyAuthentication yes/'  /etc/ssh/sshd_config
#sed -i 's/.*AuthorizedKeysFile \%h\/\.ssh\/authorized_keys/'  /etc/ssh/sshd_config
#sed -i 's/.*RhostsRSAAuthentication.*/RhostsRSAAuthentication no/'  /etc/ssh/sshd_config
#sed -i 's/.*HostbasedAuthentication.*/HostbasedAuthentication no/'  /etc/ssh/sshd_config
#sed -i 's/.*PermitEmptyPasswords.*/PermitEmptyPasswords no/'  /etc/ssh/sshd_config
#sed -i 's/.*UseLogin.*/UseLogin no/'  /etc/ssh/sshd_config
#chmod 700 ~/.ssh/
#chmod 600 ~/.ssh/authorized_keys
#systemctl restart ssh
# add my open key

echo 'set background=dark'  >>  /etc/vimrc
echo 'set background=dark'  >>  /etc/vim/vimrc

# vim
#viminfo
#plugins

# tmux 
#cp tmux conf


# fail2ban

# bash-completion
# /usr/share/bash-completion/completions/
