#!/usr/bin/env bash
#
# DESCRIPTION
# Тестовое задание
# Скрипт для установки/удаления/обновления программы из пакета rpm/deb.
# Последнюю версию можно скачать командой git clone https://github.com/celsiys/scripts.git
# Для использования нужно сделать скрипт исполняемым командой chmod +x script1.sh
# Запуск должен выполняться командой:
# sudo ./script1.sh [install|remove|status|reload] package.[deb|rpm] [%service_name%]  
# Пункты ТЗ расположены в скрипте рядом с релевантным кодом для удобства проверки.
# 
# Ограничения приведенного решения / требования:
# Tребуется /bin/bash.
# Tолько для debian-like ([ku|lu]ubuntu/kali/raspbian/mint/BolgenOS) и redhat-like (centos/fedora).
# подразумевается, что пакет собран так, что сервис создается при установке пакета и имя сервиса задается пользователем. api сервиса стандартное для sysV: start/stop/status.
# 
# Изменения:
# 
# Большая часть сообщений системы заменена на собственные. добавлены сообщения для администратора для облегчения дебага.
# Добавлены обработки если пользователь ошибся в синтаксисе с предложением правильного синтаксиса.
# Добавлены разные коды выхода.
# Протестировано с разным количеством данных на входе.
# 
# Добавить предупреждение при попытке установить deb пакет на redhat-based системе и наоборот. Предложить конвертировать с alien. теперь в качестве имени берется первое слово имени файла до первого символа  "-" "_" "+"  ".". Пробел считается разделителем параметров.
# 	например если файл называется "htop_2.0.2-1_amd64.deb"    в качестве имени будет использоваться "htop"

# 	5. Скрипт должен логировать свои действия и иметь help. 
	exec > >(tee -a log.txt)  2>&1     # write log 

release=$(lsb_release -a 2>/dev/null)
have_service=2

#check requirements and help user to avoid mistakes

# check root privileges
	if [ $(whoami) != "root" ]; then     
		echo 'use please "sudo script1.sh %action% %your_packet% %service_name%"'
		exit 2
	fi
	
	if [ -z "$1" ] ; then  echo 'use please "sudo script1.sh %action% %your_packet% %service_name%"' ; echo "user did not enter command" >> log.txt; exit 10 ; fi
	if [ -z "$2" ] ; then  echo 'use please "sudo script1.sh %action% %your_packet% %service_name%"' ; echo "user did not enter packet name" >> log.txt; exit 3 ; fi
	if [ $1 = "status" ] ; then service_name=$2  
	else
		if [ -z "$3" ] && [ $1 != "remove" ] ; then echo "you did not enter service name" 
			echo "please choose:
			1) if your packet have no service
			2) if you want to enter name 
			any else humber) to exit"	
			read have_service
			case $have_service in
				1) ;;
				2) echo "enter name of service"	& read -e service_name ;;
				*) echo "user did not enter service name" >> log.txt;  exit 4;;
			esac
		else service_name=$3
		fi
	fi

# check supported distro
	if [ -f /etc/centos-release ] || [ -f /etc/redhat-release ] ||  grep -iq redhat /etc/os-release  || [ -f /etc/fedora-release ] ; then  
 		install="yum -y localinstall"
		remove="yum -y remove"
		update="yum -y localupdate"
	elif [ -f /etc/debian-release ] ||  grep -iq debian /etc/os-release ||  grep -iq ubuntu /etc/os-release ; then
		install="dpkg -i" 
		remove="apt-get -y remove"
		update="dpkg -i"
	elif [ -f /etc/gentoo-release ] ; then
		echo "Do not use my ugly script. Teach me!(c)"
		exit 5
	else
		echo "Unsupported distro, sorry. You can send feature-request to admin. q@q.q"
		echo -e "Your distro is \n$release"
		# add check mail agent 
		exit 6;
	fi


# strip name
	name=$(basename "$2" | sed 's/[\.\+\-\_\-].*//')

# crutch for removing packets
# script need this direcrory cuz "apt remove packet*"  try to find user defined packet.deb in system instead of packet_version
dir1=$RANDOM$RANDOM

#	1.Для любой Unix-like системы необходимо написать скрипт(ы) для установки/удаления/обновления заведомо неизвестных нам пакетов из файлов (.rpm или .deb или любых других на Ваше усмотрение). 
# 	2. После установки пакетов, необходимо перезапускать сервис, имя которого передается как параметр. 
	case $1 in
		install)
# 	4. Предусмотреть в скрипте исключительные ситуации, например, пакет(ы) уже установлен(ы). 
			if [ -x "$(command -v $name)" ] ; then echo 'Package already installed, use "sudo ./script1.sh update %your_packet%" to update or "sudo ./script1.sh status %your_service%" to check whether your service is running'
			exit 7
			else 
# 	3. Проверить, что пакеты установлены, а сервис запущен. 
				$install $2 >>log.txt  2>&1
				if  [ -x "$(command -v $name)" ] ; then 

					if [ $have_service -eq 1 ]; then echo "packet has installed, service not needed"; exit 11; fi

					if ps aux | grep $service_name | grep -v grep | grep -v script1.sh >>log.txt 2>&1 ; then echo "your packet has installed and service is running"
					else echo "your packet has installed, but service has not started, see log.txt for causes"
					fi

				else echo "packet has not installed, see log.txt for causes"
				fi
			fi ;; 
		remove)	if [ -x "$(command -v $name)" ] ; then 
					mkdir $dir1 ; cd  $dir1  
					$remove $name* >>log.txt  2>&1
					cd .. ;  rm -rf $dir1 ; echo "packet removed" 
				else echo "packet does not installed"
				fi;;
		update) $update $2 > /dev/null 2>&1;;	
		reload) if  [ -x "$(command -v $name)" ] ; then service $service_name stop; service $service_name start; 
					if ps aux | grep $service_name | grep -v grep | grep -v script1.sh >> log.txt  2>&1; then echo "your service is running"
					else echo "your service has not started, see log.txt for causes"
					fi
				else echo 'packet has not installed, use please "sudo script1.sh install %your_packet% %service_name% '
				fi ;;
		status) service $service_name status >> log.txt  2>&1
					if ps aux | grep $service_name | grep -v grep | grep -v script1.sh  >> log.txt  2>&1; then echo "your service is running"
					else echo "your service has not started, see log.txt for causes"
					fi ;;	
		*) echo "unknown command. Usage: sudo ./script1.sh {install|remove|update|reload|status} %your_packet% %service_name%"
		exit 8 
		;;
	esac

exit 9



# 	6. Также на Ваше усмотрение реализуйте в скрипте любую интересную функцию. Например, создание бэкапов или «пасхалку». 
# 	7. Дистрибутив, пакетный менеджер, сервис остаются на Ваше усмотрение. Нет строгих ограничений по реализации скрипта.

