cmake_minimum_required(VERSION 3.2)
project(ubung1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
    .idea/.name
    .idea/encodings.xml
    .idea/misc.xml
    .idea/modules.xml
    .idea/ubung1.iml
    .idea/vcs.xml
    .idea/workspace.xml
    CMakeLists.txt
    main.cpp
    mel_bibliotek.h
    mel_header.h)

add_executable(ubung1 ${SOURCE_FILES})