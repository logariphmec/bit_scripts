# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


PATH=$PATH:/usr/local/go/bin

HISTCONTROL=erasedups #=ignoreboth
PROMPT_COMMAND='history -a'      # save history from multiple tabs 
HISTSIZE=100000
HISTFILESIZE=100000
#HISTTIMEFORMAT="%h.%d %H:%M:%S "   # add timestamp to commands
HISTIGNORE="history*:&:[bf]g"

PS1='\[\e[1;33m\]\[\e[m\]\[\e[1;33m\]\w\[\e[m\] \[\e[1;33m\]\$\[\e[m\] '

#if [ -f "$(which powerline-daemon)" ]; then
#  powerline-daemon -q
#  POWERLINE_BASH_CONTINUATION=1
#  POWERLINE_BASH_SELECT=1
#  . /usr/share/powerline/bindings/bash/powerline.sh
#fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm|xterm-color|*-256color) color_prompt=yes;;
esac



shopt -s autocd      # dont need to type cd to change dir 
shopt -s checkwinsize   # update the values of LINES and COLUMNS after each command
shopt -s cdspell        # correct mistakes in path in command cd
shopt -s cmdhist ## save all lines of a multiple-line command in the same history entry (allows easy re-editing of multi-line commands)
shopt -s dirspell ## If set, Bash attempts spelling correction on directory names during word completion if the directory name initially supplied does not exist.
shopt -s histappend    # append to the history file, don't overwrite it
#shopt -s globstar # # If set, the pattern "**" used in a pathname expansion context will match all files and zero or more directories and subdirectories.

export ANSIBLE_NOCOWS=1
#export MANPAGER="/usr/bin/most -s"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

backup() { cp "$1"{,.bak};}
packg() { tar -cz "$1" | gpg --symmetric --cipher-algo AES256 -o "$1".tgz.gpg;}
unpackg() { gpg -d "$1" | tar xz;}
cddir() { cd "$(dirname "$1")" || exit 1;} 



  
div() {
	local columns=$(($(tput cols) - 8))
	local line=$(printf '%0.1s' "-"{1..500})
	printf "\e[01;31m---- 8< ${line:0:${columns}}\e[0m\n"
}


alias rm='rm -vI'
alias ls='ls --color=auto'
alias diff='colordiff'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias l='ls -AlhF --color=auto --group-directories-first'
alias psg="ps aux | grep -i"
alias dpkgg="dpkg -l | grep -i"
alias df="df -h"
alias du="du -hd 1"
#alias dumb='du -shx * | sort -rhk1'    # показывает диск. простр. исп. папками, сначала бОльшие
alias pingg='ping google.com'
alias ping='grc ping'
alias myip='curl -s checkip.dyndns.org | sed -e "s/.*Current IP Address: //" -e "s/<.*$//"' 
alias mountt="mount | column -t"
alias less='less -FXR'


alias tr1='tr -dc ACDEFGHKMNPQRSTUWXYZabcdefhkmnpqrstuwxyz2-79 < /dev/urandom  | head -c16| xargs'
alias tr2='tr -dc ACDEFGHKMNPQRSTUWXYZabcdefhkmnpqrstuwxyz2-79\_\$\#\%\&\(\)* < /dev/urandom  | head -c16| xargs'

alias stest2="ssh ${TEST_IP2}"
alias stest="ssh ${TEST_IP1}"
alias sdock='ssh -t ${DOCKER1_IP} "tmux a"'
alias sdock2='ssh -t ${DOCKER2_IP} "tmux a"'
alias jcli="java -jar /hobby/soft/jenkins-cli.jar -s http://localhost:8083/ $* --username 1 --password 1"
alias jenk="java -jar /hobby/soft/jenkins.war &"

#docker 
alias cddo='cd /hobby/vm/docker'
alias dcr='docker rm $(docker ps -a -q -f status=exited)'
alias di='docker images -a'
alias dpa="docker ps -a"
#alias dlg="docker logs -f $1"
#alias dkd="docker run -d -P"
#alias dki="docker run -i -t -P"
#alias dkid="docker run -d -i -t -P"
#alias dex="docker exec -i -t"
alias dstop='docker stop $(docker ps -a -q)'
alias drm='docker rm $(docker ps -a -q)'
alias dri='docker rmi $(docker images -q)'
#alias drmf='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'
#alias dbu='docker build -t'
alias dalias='alias|grep docker'
alias dip="docker inspect --format '{{ .NetworkSettings.IPAddress }}'"
alias dpid="docker inspect --format '{{ .State.Pid }}'"
nse () { nsenter --target "$(docker inspect --format "{{.State.Pid}}" "$1")" --mount --uts --ipc --net --pid ;}
#alias nsent='nsenter -m -u -n -p -i -t "$@"'

# git
#alias g='git'
alias ga='git add .'
alias gp='git push'
alias gl='git log --oneline --graph'
#alias gs='git status'
#gsb # git status -sb
#gaa # git add --all
#gdca # git diff --cached
#gru # git reset --
#gdw # git diff --word-diff  #   diff  по словам вместо строк
#gau # git add --update  # добавлять в индекс только файлы, уже находящиеся под наблюдением Гита: 
#gc! # git commit -v --amend     #  -v покажет ещё и diff
#gcm # git checkout master
#gp # git push
#git branch --no-color --merged | command grep -vE "^(\*|\s*(master|develop|dev)\s*$)" | command xargs -n 1 git branch -d   # удалить смерженные ветки
#alias gd='git diff'
#alias gm='git commit -m'
#alias gmc='git commit --amend'
#alias gma='git commit -am'
#alias gb='git branch'
#alias gc='git checkout'
#alias gra='git remote add'
#alias grr='git remote rm'
#alias gpu='git pull'
#alias gcl='git clone'



# python
#export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
#source /usr/local/bin/virtualenvwrapper.sh

## python virtualenv
#if [ -f /usr/local/bin/virtualenvwrapper.sh ]; then
#	export PROJECT_HOME=~/work/
#	export WORKON_HOME=~/work/.venv/
#	export VIRTUAL_ENV_DISABLE_PROMPT=1
#	source /usr/local/bin/virtualenvwrapper.sh
#fi
#
## Update python path
## XXX: this is necessary only for local machine
##[[ -d "/usr/local/lib/python2.7/site-packages" ]] && export PYTHONPATH="/usr/local/lib/python2.7/site-packages:$PYTHONPATH"
#



#alias ss8h="echo ${CISCO_VPN_PASSWORD} | openconnect -b -s  /usr/share/vpnc-scripts/vpnc-script --user=${CISCO_VPN_USER} ${CISCO_VPN_SERVER}/client --servercert sha256:${CISCO_VPN_SHA}"
alias ss8d='kill -2 $(ps aux | grep openconn | grep -v grep |  tr -s " " | cut -d " " -f 2); echo "nameserver 8.8.8.8" >> /etc/resolv.conf'

findd (){
find -name "*$1*"
}



# color for man
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[07;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'



# crunch for aws bash-completion
complete -C /usr/bin/aws_completer aws


#alias cm="cryptsetup luksOpen /hobby/container.img container; mount /dev/mapper/container /hobby/drop"
#alias cu="umount /hobby/drop; cryptsetup luksClose /dev/mapper/container"

#alias hm="sudo cryptsetup luksOpen /dev/sda5 sda5 ; sudo mount -t ext4 /dev/mapper/sda5 /hobby"
#alias hu="sudo umount  /hobby ; sudo cryptsetup luksClose sda5"

#color_prompt=yes
#force_color_prompt=yes
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'







## ~/.bashrc: executed by bash(1) for non-login shells. see /usr/share/doc/bash/examples/startup-files (in the package bash-doc) for examples
#
## If not running interactively, don't do anything
#[ -z "$PS1" ] && return
#
## unset this because of nasty OS X bug with annoying message:
## "dyld: DYLD_ environment variables being ignored because main executable (/usr/bin/sudo) is setuid or setgid"
## this is not correct, but Apple is too lazy to fix this
#unset DYLD_LIBRARY_PATH
#
## add local bin path
#PATH=$HOME/.bin:$PATH
#PATH=/usr/local/bin:$PATH
#PATH=/usr/local/sbin:$PATH
#[ -d /usr/local/mysql/bin ] && PATH=/usr/local/mysql/bin:$PATH
#[ -d /usr/local/share/npm/bin ] && PATH=/usr/local/share/npm/bin:$PATH
#
#
## setup color variables
#color_is_on=
#color_red=
#color_green=
#color_yellow=
#color_blue=
#color_white=
#color_gray=
#color_bg_red=
#color_off=
#color_user=
#if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
#	color_is_on=true
#	color_black="\[$(/usr/bin/tput setaf 0)\]"
#	color_red="\[$(/usr/bin/tput setaf 1)\]"
#	color_green="\[$(/usr/bin/tput setaf 2)\]"
#	color_yellow="\[$(/usr/bin/tput setaf 3)\]"
#	color_blue="\[$(/usr/bin/tput setaf 6)\]"
#	color_white="\[$(/usr/bin/tput setaf 7)\]"
#	color_gray="\[$(/usr/bin/tput setaf 8)\]"
#	color_off="\[$(/usr/bin/tput sgr0)\]"
#
#	color_error="$(/usr/bin/tput setab 1)$(/usr/bin/tput setaf 7)"
#	color_error_off="$(/usr/bin/tput sgr0)"
#
#	# set user color
#	case `id -u` in
#		0) color_user=$color_red ;;
#		*) color_user=$color_green ;;
#	esac
#fi
#
## some kind of optimization - check if git installed only on config load
#PS1_GIT_BIN=$(which git 2>/dev/null)
#
#if [ -f ~/.hostname ]; then
#	LOCAL_HOSTNAME=`cat ~/.hostname`
#else
#	LOCAL_HOSTNAME=$HOSTNAME
#fi
#
#function prompt_command {
#	local PS1_GIT=
#	local PS1_VENV=
#	local GIT_BRANCH=
#	local GIT_DIRTY=
#	local PWDNAME=$PWD
#
#	# beautify working directory name
#	if [[ "${HOME}" == "${PWD}" ]]; then
#		PWDNAME="~"
#	elif [[ "${HOME}" == "${PWD:0:${#HOME}}" ]]; then
#		PWDNAME="~${PWD:${#HOME}}"
#	fi
#
#	# parse git status and get git variables
#	if [[ ! -z $PS1_GIT_BIN ]]; then
#		# check we are in git repo
#		local CUR_DIR=$PWD
#		while [[ ! -d "${CUR_DIR}/.git" ]] && [[ ! "${CUR_DIR}" == "/" ]] && [[ ! "${CUR_DIR}" == "~" ]] && [[ ! "${CUR_DIR}" == "" ]]; do CUR_DIR=${CUR_DIR%/*}; done
#		if [[ -d "${CUR_DIR}/.git" ]]; then
#			# 'git repo for dotfiles' fix: show git status only in home dir and other git repos
#			if [[ "${CUR_DIR}" != "${HOME}" ]] || [[ "${PWD}" == "${HOME}" ]]; then
#				# get git branch
#				GIT_BRANCH=$($PS1_GIT_BIN symbolic-ref HEAD 2>/dev/null)
#				if [[ ! -z $GIT_BRANCH ]]; then
#					GIT_BRANCH=${GIT_BRANCH#refs/heads/}
#
#					# get git status
#					local GIT_STATUS=$($PS1_GIT_BIN status --porcelain 2>/dev/null)
#					[[ -n $GIT_STATUS ]] && GIT_DIRTY=1
#				fi
#			fi
#		fi
#	fi
#
#	# build b/w prompt for git and virtual env
#	[[ ! -z $GIT_BRANCH ]] && PS1_GIT=" (git: ${GIT_BRANCH})"
#	[[ ! -z $VIRTUAL_ENV ]] && PS1_VENV=" (venv: ${VIRTUAL_ENV#$WORKON_HOME})"
#
#	# calculate prompt length
#	local PS1_length=$((${#USER}+${#LOCAL_HOSTNAME}+${#PWDNAME}+${#PS1_GIT}+${#PS1_VENV}+3))
#	local FILL=
#
#	# if length is greater, than terminal width
#	if [[ $PS1_length -gt $COLUMNS ]]; then
#		# strip working directory name
#		PWDNAME="...${PWDNAME:$(($PS1_length-$COLUMNS+3))}"
#	else
#		# else calculate fillsize
#		local fillsize=$(($COLUMNS-$PS1_length))
#		FILL=$color_white
#		while [[ $fillsize -gt 0 ]]; do FILL="${FILL}-"; fillsize=$(($fillsize-1)); done
#		FILL="${FILL}${color_off}"
#	fi
#
#	if $color_is_on; then
#		# build git status for prompt
#		if [[ ! -z $GIT_BRANCH ]]; then
#			if [[ -z $GIT_DIRTY ]]; then
#				PS1_GIT=" (git: ${color_green}${GIT_BRANCH}${color_off})"
#			else
#				PS1_GIT=" (git: ${color_red}${GIT_BRANCH}${color_off})"
#			fi
#		fi
#
#		# build python venv status for prompt
#		[[ ! -z $VIRTUAL_ENV ]] && PS1_VENV=" (venv: ${color_blue}${VIRTUAL_ENV#$WORKON_HOME}${color_off})"
#	fi
#
#	# set new color prompt
#	PS1="${color_user}${USER}${color_off}@${color_yellow}${LOCAL_HOSTNAME}${color_off}:${color_black}${PWDNAME}${color_off}${PS1_GIT}${PS1_VENV} ${FILL}\n➜ "
#
#	# get cursor position and add new line if we're not in first column
#	# cool'n'dirty trick (http://stackoverflow.com/a/2575525/1164595)
#	# XXX FIXME: this hack broke ssh =(
##	exec < /dev/tty
##	local OLDSTTY=$(stty -g)
##	stty raw -echo min 0
##	echo -en "\033[6n" > /dev/tty && read -sdR CURPOS
##	stty $OLDSTTY
#	echo -en "\033[6n" && read -sdR CURPOS
#	[[ ${CURPOS##*;} -gt 1 ]] && echo "${color_error}↵${color_error_off}"
#
#	# set title
#	echo -ne "\033]0;${USER}@${LOCAL_HOSTNAME}:${PWDNAME}"; echo -ne "\007"
#}
#
## set prompt command (title update and color prompt)
#PROMPT_COMMAND=prompt_command
## set new b/w prompt (will be overwritten in 'prompt_command' later for color prompt)
#PS1='\u@${LOCAL_HOSTNAME}:\w\$ '
#
## Postgres won't work without this
#export PGHOST=/tmp
#
## grep colorize
#export GREP_OPTIONS="--color=auto"
#
## bash completion
#if [ -f /usr/local/bin/brew ]; then
#	if [ -f `/usr/local/bin/brew --prefix`/etc/bash_completion ]; then
#		. `/usr/local/bin/brew --prefix`/etc/bash_completion
#	fi
#fi
#
## bash aliases
#if [ -f ~/.bash_aliases ]; then
#	. ~/.bash_aliases
#fi
#
## this is for delete words by ^W
#
#encrypt()
#{
#  echo "Encrypting $1..."
#  openssl enc -aes-256-cbc -salt -a -in $1 -out $2 || { echo "File not found"; return 1; }
#  echo "Successfully encrypted"
#}
#
### uses openssl aes 256 cbc decryption to decrypt file
#decrypt()
#{
#  echo "Decrypting $1..."
#  openssl enc -aes-256-cbc -d -a -in $1 -out $2 || { echo "File not found"; return 1; }
#  echo "Successfully decrypted"
#}
#alias ..='cd ..'
#alias ...='cd ../../'
#alias ....='cd ../../..'
#
#alias mount='mount |column -t'
#alias ct='column -t'
#alias lsock='sudo /usr/sbin/lsof -i -P'
#alias tn='tr -d "\n"'
#alias path="echo $PATH | tr -s ':' '\n'"
#alias msf='msfconsole -q'
#alias os='openstack'
#alias fuck='$(thefuck $(fc -ln -1))'
#alias hgrep='history | grep $1'
#alias lm='ls -latr'
#alias ll='ls -l --color=auto'
#alias la='ls -A --color=auto'
#alias l='ls -CF --color=auto'
#alias vi='vim'
#alias t='tree -L 1 -C -h'
#alias week='date +%V'
#alias wget='wget --no-check-certificate'
#alias wsite='wget --no-check-certificate --random-wait -r -p -e robots=off -U mozilla'
#alias scp='scp -oStrictHostKeyChecking=no'
#alias ssh='ssh -oStrictHostKeyChecking=no'
#alias hosts='sudo vim /etc/hosts'
#alias open='/usr/bin/chromium-browser "$@"'
#alias ccat='pygmentize -O bg=dark,style=colorful'



