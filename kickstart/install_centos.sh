#!/usr/bin/env bash
set -ex

## default values variables
IP_ADDRESS=${1:-192.168.122.100}
HOST_NAME=${2:-centos1}
VM_NAME=$HOST_NAME
MEM_SIZE=${3:-1024}
VCPUS=${4:-1}
OS_TYPE=${5:-linux}
ISO_FILE=${6:-centos-7-x86_64-Minimal-1804.iso}
DISK_SIZE=${7:-4}
OS_VARIANT=${8:-rhel7}


# update kickstart file
cp ks.cfg ks.cfg.bak
#echo -en "Enter Hostname: "
#read HOST_NAME
echo "HOST_NAME is $HOST_NAME"
#echo -en "Enter IP Address: "
#read IP_ADDRESS
echo IP_ADDRESS is $IP_ADDRESS
sed -i 's/server1/'$HOST_NAME'/g' ks.cfg.bak
sed -i 's/192.168.122.100/'$IP_ADDRESS'/g' ks.cfg.bak
sed -i 's/192.168.122.100/'$IP_ADDRESS'/g' host.yml
cp ks.cfg.bak /var/www/html/ks.cfg
#echo -en "Enter vm name: "
#read VM_NAME
echo "VM_NAME is $VM_NAME"
#echo -en "Enter virtual disk size : "
#read DISK_SIZE
echo "DISK_SIZE is $DISK_SIZE"
 
virt-install \
     --name ${VM_NAME} \
     --memory=${MEM_SIZE} \
     --vcpus=${VCPUS} \
     --os-type ${OS_TYPE} \
     --location ${ISO_FILE} \
     --disk size=${DISK_SIZE}  \
     --network bridge=virbr0 \
     --graphics=none \
     --os-variant=${OS_VARIANT} \
     --console pty,target_type=serial \
     -x 'console=ttyS0,115200n8 serial' \
     -x "ks=http://192.168.122.1/ks.cfg" 
