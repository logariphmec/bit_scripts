#!/bin/bash
set -ex 

vmname=newvm
vmdomain=test.lab
disksize=3
vcpus=1
ram=1024
vgname=vgdata
vmipaddress=192.168.122.10
vmnetmask=255.255.255.0
vmgateway=192.168.122.10
vmbridge=virbr0
dnsserveraddress=192.168.122.1


virt-install -n vmname -r 1048 --os-type=linux --os-variant=debian9 --disk size=3 -w bridge=virbr0,model=virtio --vnc --noautoconsole -c /path/to/debian-9.5.0-amd64-netinst.iso 

#virt-install -n vmname -r 2048 --os-type=linux --os-variant=ubuntulucid  --disk  /dev/vg_name/lv_name,device=disk,bus=virtio  -w bridge=br0,model=virtio --vnc --noautoconsole -c  /kvm/images/iso/ubuntu.iso

#virt-install -n vmname -r 2048 --os-type=linux  --os-variant=ubuntulucid --disk  /kvm/images/disk/vmname_boot.img,device=disk,bus=ide,size=40,sparse=true,format=raw  -w bridge=br0,model=e1000 --vnc --noautoconsole -c  /kvm/images/iso/ubuntu.iso

#virt-install -n vmname -r 2048 --os-type=linux --os-variant=ubuntulucid --disk /kvm/images/disk/vmname_boot.img,device=disk,bus=virtio,size=40,sparse=true,format=raw --disk /kvm/images/iso/ubuntu.iso,device=cdrom -w bridge=br0,model=virtio --vnc --noautoconsole

#--disk path=/dev/mapper/${vgname}-lv${vmname},bus=virtio \
#--location=http://ftp.us.debian.org/debian/dists/stable/main/installer-amd64/ \
#--location=/mnt/1/debian/ \
#-c /path/to/debian-9.5.0-amd64-netinst.iso \

#virt-install \
# --name=${vmname} \
# --vcpus=${vcpus} \
# --ram=${ram} \
# --os-type=linux \
# --os-variant=debian9 \
# --disk size=3 \
# --accelerate \
# --graphics vnc \
# --noautoconsole \
# --wait=-1 \
# --hvm \
# --autostart \
# --network bridge=${vmbridge},model=virtio \
 #--extra-args="auto=true priority=critical vga=788 languagechooser/language-name=English countrychooser/shortlist=US console-keymaps-at/keymap=en netcfg/disable_autoconfig=true netcfg/disable_dhcp=true netcfg/choose_interface=eth0 netcfg/get_ipaddress=${vmipaddress} netcfg/get_netmask=${vmnetmask} netcfg/get_gateway=${vmgateway} netcfg/get_nameservers=${dnsserveraddress} hostname=${vmname} netcfg/get_domain=${vmdomain} initrd=install.amd/initrd.gz -- preseed/url=http://localhost/preseed.cfg"


#virt-install --name=${vmname} --vcpus=${vcpus} --ram=${ram} --os-type=linux --os-variant=ubuntuprecise --disk path=/dev/mapper/${vgname}-lv${vmname},bus=virtio --accelerate --graphics vnc --noautoconsole --wait=-1 --hvm --autostart --network bridge=${vmbridge},model=virtio --location=http://webserver.test.lab/ubuntu/dists/precise/main/installer-amd64/ --extra-args="auto=true priority=critical vga=788 languagechooser/language-name=English countrychooser/shortlist=US console-keymaps-at/keymap=en netcfg/disable_autoconfig=true netcfg/disable_dhcp=true netcfg/choose_interface=eth0 netcfg/get_ipaddress=${vmipaddress} netcfg/get_netmask=${vmnetmask} netcfg/get_gateway=${vmgateway} netcfg/get_nameservers=${dnsserveraddress} DEBCONF_DEBUG=developer hostname=${vmname} netcfg/get_domain=${vmdomain} initrd=ubuntu/precise-amd64/initrd.gz -- preseed/url=http://webserver.test.lab/ubuntu-amd64-preseed.txt"
