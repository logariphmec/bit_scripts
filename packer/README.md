Quick start

    Install Packer.
    Add your AWS credentials as the environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.
    Run packer build simple-web-server.json.
    When the build is done, it'll output the ID of an AMI in us-east-1 that you can deploy. See the terraform-example-full folder for an example of how to deploy this AMI and run the Ruby on Rails app within it.

