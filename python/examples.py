#!/usr/bin/python3
# -*- coding: utf-8 -*-
import subprocess
from re import findall
from collections import Counter
import math
import sys
import requests


## work with environments
#from dotenv import load_dotenv
#load_dotenv()
#
## OR, the same with increased verbosity:
#load_dotenv(verbose=True)
#
## OR, explicitly providing path to '.env'
#from pathlib import Path  # python3 only
#env_path = Path('.') / '.env'
#load_dotenv(dotenv_path=env_path)
#
#import os
#SECRET_KEY = os.getenv("EMAIL")
#DATABASE_PASSWORD = os.getenv("DATABASE_PASSWORD")



#import feedparser
#import time
#from gtts import gTTS
#import os
#import pyowm
#
#rss   speech 
#news = feedparser.parse("http://feeds.bbci.co.uk/news/rss.xml?edition=uk")
#pir = MotionSensor(17)
#while True:
#    status = pir.wait_for_inactive()
#    if status == True:
#        current_time = time.ctime()
#        str(current_time)
#        tts = gTTS(text=(current_time), lang='en-us')
#        tts.save("time.mp3")
#        os.system("mpg321 time.mp3")
#        for i in range(5):
#            tts = gTTS(text=(news["entries"][i]["title"]), lang="en-us")
#            tts.save("news.mp3")
#            os.system("mpg321 news.mp3")
#        owm = pyowm.OWM("API KEY")
#        observation = owm.weather_at_place("Blackpool,uk")
#        w = observation.get_weather()
#        weather = w.get_status()
#        tts = gTTS(text=("The current weather is, "+str(weather)), lang="en-us")
#        tts.save("weather.mp3")
#        os.system("mpg321 weather.mp3")
#        temperature = w.get_temperature("celsius")["temp_max"]
#        tts = gTTS(text=("The current temperature is, "+str(temperature)+"celsius"), lang="en-us")
#        tts.save("temperature.mp3")
#        os.system("mpg321 temperature.mp3")
#
#
#
#url='http://www.highload.ru/moscow/2017/schedule'
#print (url)
#
#print(requests.get('http://www.highload.ru/moscow/2017/schedule'))
#

#print(len(requests.get('url').text.splitlines()))



#a=''
#count=1
#with open('dataset_3378_3.txt' ,'rt') as f:
#  for line in f:
#    file_full=line.strip()
#r=requests.get (file_full)
#file=file_full[52:]
#file='452488.txt'
#url='https://stepic.org/media/attachments/course67/3.6.3/'+file
#print (url)
#r=requests.get (url)
#print (r.text)
#while r.text[0:1] != 'We':
#  url_full='https://stepic.org/media/attachments/course67/3.6.3/'+r.text
#  r=requests.get (url_full)
#  print (r.text)
#  count+=1
#  print (count)
#print (url)
##    a=requests.get ('https://stepic.org/media/attachments/course67/3.6.3/file')
#while a!='We':



#print(len(requests.get(file).text.splitlines()))
#print(len(requests.get('https://stepic.org/media/attachments/course67/3.6.2/316.txt').text.splitlines()))

#print (a.splitlines())



#url='http://ya.ru'
#par= {'key1':'value1','key2':'value2'}
#r= requests.get (url, params=par, cookies=cookies)
#print (r.text)
#
#
#a=sys.argv
#a.pop(0)
#
#[print (i,end=' ') for i in a]
#
#
#
## допустим, у нас есть список фруктов, где зафиксированы самые низкие и высокие цены на эти фрукты
## т.е. по сути это список списков :)
#lst = [["apple", 55, 62], ["orange", 60, 74], ["pineapple", 140, 180], ["lemon", 80, 84]]
#
## выведем этот список для нагляности на экран, используя [list comprehension]
#[print(el) for el in lst]
## ['apple', 55, 62]
## ['orange', 60, 74]
## ['pineapple', 140, 180]
## ['lemon', 80, 84]
#
## если мы хотим подсчитать среднюю цену на каждый из фруктов, то напишем что-то вроде
#sumMiddle = 0
#for el in lst:
#    sumMiddle = (el[1] + el[2]) / 2
#    print(sumMiddle)
#
## или можно сделать это одной строкой
#[print((priceLow + priceHigh) / 2) for fruit, priceLow, priceHigh in lst]
## представьте, что наш список списков - это таблица из трёх столбцов
## и мы можем обращаться к столбцам, просто озаглавив их fruit, priceLow, priceHigh
## в цикле for, почти как перебор элементов словаря for key, value in d.items() :)
#
## поэтому, когда вы захотите прикинуть, сколько же, от и до, в среднем может стоить
## ваша фруктовая корзина, нужно будет посчитать среднее по каждой колонке
## вы можете сделать это примерно так
#sumLow, sumHigh = 0, 0
#for el in lst:
#    sumLow += el[1]
#    sumHigh += el[2]
#sumLow /= len(lst)
#sumHigh /= len(lst)
#print(sumLow, sumHigh)
#
## или применить кунг-фу списковых выражений и обойтись парой строк :)
#print(sum([priceLow for fruit, priceLow, priceHigh in lst]) / len(lst))
#print(sum([priceHigh for fruit, priceLow, priceHigh in lst]) / len(lst))
#


# Отличное объяснение, разложено по полочкам!
# # Хочу немного дополнить.
#
# # Чтобы не ограничиваться длиной строки в матрице, например
# # ['apple', 55, 62, 49, ..., priceN]
# # ['orange', 60, 74, 86, ..., priceN]
# # ['pineapple', 140, 180, 192, ..., priceN]
# # ['lemon', 80, 84, 79, ..., priceN]
#
# # И соответственно, не плодить priceLow, priceHigh, ..., priceN,
# # можно слегка изменить код. 
# # Вместо этого:
# ## или можно сделать это одной строкой 
# ## [print((priceLow + priceHigh) / 2) for fruit, priceLow, priceHigh in lst]
#
# # используем Extended Iterable Unpacking (*args):
# print( *[sum(price)/len(price) for fruit,*price in lst], sep='\n' )
#
# # И вместо кучи print-ов:
# ## print(sum([priceLow for fruit, priceLow, priceHigh in lst]) / len(lst))"
# ## print(sum([priceHigh for fruit, priceLow, priceHigh in lst]) / len(lst))"
# ## ...
# ## print N sum
#
# # тоже используем *args:
# for i in range(1, len(lst[0])):
#     print( sum([price[i] for [*price] in lst]) / len(lst) )
#
#
#
#
#
#a,b,c,student_count=0,0,0,0
#with open('dataset_3363_4.txt' ,'rt') as f:
#  for line in f:
#    #print (line.replace('\n', '').split(';'))
#    x=line.replace('\n', '').split(';')
#    print ((int(x[1])+int(x[2])+int(x[3]))/3)
#    a+=int(x[1])
#    b+=int(x[2])
#    c+=int(x[3])
#    student_count+=1
#print (a/student_count, b/student_count, c/student_count)

#import re
#line = re.sub(r"</?\[\d+>", "", line)
#
#line = re.sub(r"""
#  (?x) # Use free-spacing mode.
#  <    # Match a literal '<'
#  /?   # Optionally match a '/'
#  \[   # Match a literal '['
#  \d+  # Match one or more digits
#  >    # Match a literal '>'
#  """, "", line)
#

#
#text = open("dataset_3363_3.txt", 'r')
#words = text.read().replace('\n', ' ').lower().split()
#text.close()
#
#print ('words')
#print (words)
#words.sort()
#print ('words.sorted')
#print (words)
#count=[]
#b=0
#for word in words:
#  count=(words.count(word))
#  print (word, count)
#  if count > b:
#    b=count
#    print (words[b], b)
#
#print (count, b )
#print (words[b])
#print (Counter (words))
#  





#text = open("dataset_3363_3.txt", 'r')
#words = text.read().replace('\n', ' ').lower().split()
#text.close()
#words.sort()
#count=[]
#b=0
#for word in words:
#  count=(words.count(word))
#  print (word, count)
#  if count > b:
#    b=count
#    print (words[b], b)
#
#print (count, b )
#print (words[b])
#print (Counter (words))
#  

#with open('dataset_3363_3.txt' ,'rt') as f:
##with open('dataset.txt' ,'rt') as f:
#  for line in f:
#    words+=' '
#    words+=line.strip()
#
#

#####print (set(sorted(s1.lower().split())))
####small=(set(sorted(s1.lower().split())))
####print (small)
####for i in small:
####	print (s2.count(i))
####	
####
####counter = {}
####for word in s2:
####	counter[word] = counter.get(word, 0) + 1
####	
####        
####max_count = max(counter.values())
####most_frequent = [k for k, v in counter.items() if v == max_count]
####print(min(most_frequent))
####
####
####



#for i in range(len(s1)):
#    if s1[i].isalpha():
#      alpha=s1[i]
#      amount='0'
#    elif s1[i+1].isdigit():
#      amount+=s1[i]
#    else:
#      amount+=s1[i]
#      print (alpha * int(amount), end='')



#s1=[]
#s1='a3b4c2e44b1'
#print(s1)
#print(s1[4])
#alpha='f'
#amount='57'
#print (amount)
#amount+=s1[4]
#print (amount)
##print (alpha * int(amount))



#a3b4c2e44b1
#amount='0'
#with open('dataset_3363_2.txt') as foo:
#  s1 = foo.readline()
##s1+='a'
#for i in range(len(s1)-1):
#    if s1[i].isalpha():
#      alpha=s1[i]
#      amount='0'
#    elif s1[i+1].isdigit():
#      amount+=s1[i]
#    else:
#      amount+=s1[i]
#      print (alpha * int(amount), end='')





    #  try:
    #    int('')
    #  except ValueError:
    #    pass      # or whatever




#>>> s1, s2 = 'Митя', 'Василиса'
#>>> '%s + %s = любовь' % (s1, s2)
#'Митя + Василиса = любовь'
#

#      import itertools
#      
#      LENGTH = 10000
#      
#      
#      def char_range(c1, c2):
#          """Generates the characters from `c1` to `c2`, inclusive."""
#          for c in range(ord(c1), ord(c2) + 1):
#              yield chr(c)
#      
#      
#      alphabet = list(char_range('A', 'Z'))
#      output = []
#      
#      extent = 1
#      while len(output) < LENGTH:
#          output += itertools.product(*([alphabet] * extent))
#          extent += 1
#      
#      output = output[0:LENGTH]
#      output = ["".join(e) for e in output]
#      
#      print(output)
#      print(len(output))
#      


#q=[]
#a='1'
#print (q) 
#q.append(a)
#print (q) 
#b='32'
#q.append(b)
#print (q) 
#
#T2 = ''.join(q)
#print (T2)
#a=int(T2)+3
#print (a)
#b=a+3
#

#T2 = [x for x in q]
#T2.join()
#print (T2)
#T2 = [list(map(int, x)) for x in q]
#q.join()

#with open('file.txt') as foo:
#  s1 = foo.readline()
#
#for i in range(0,len(s1)-1):
#    if s1[i].isdigit():
#      print (s1[i])
 #     q.append()
 #     print (q)
#  for j in range(int(s1[i+1])):
#    print (s1[i])


"""
with open('file.txt') as foo:
  s1 = foo.readline()

for i in range(0,len(s1)-1,2):
  for j in range(int(s1[i+1])):
    print (s1[i])







foo = open ('file.txt','r')
s1 = foo.readline()
foo.close() 

with open('file.txt') as foo:
  s1 = foo.readline()
  s2 = foo.readline()

bar = open ('file.txt','w')
bar.write('some text\n')
bar.write(str(25))
bar.close() 

with open('file.txt') as foo:
  s1 = foo.readline()
  s2 = foo.readline()




# lst = [1, 2, 3, 4, 5, 6]

<F12>#lst = [a, aa, abC, aa, ac, abc, bcd, a]
lst=[i for i in input().lower().split()]
#lst="a aa abC aa ac abc bcd a".lower().split()
for i in set(lst):
  print (i,' ',lst.count(i))

#for i in lst:
#  if lst[i] == lst[i-1]
#    count+=1
#  else:
#    print (i,' ',count)
#    count=0

#def update_dictionary(d, key, value):
#    if key in d:
#        d[key].append(value)
#    elif key * 2 not in d:
#        d[key * 2] = [value]
#    else:
#        d[key * 2].append(value)
#    #return d.get(key)
#    return d
#
#
#d = {}
#print(update_dictionary(d, 1, -1))  # None
#print(d)  # {2: [-1]}
#update_dictionary(d, 2, -2)
#print(d)  # {2: [-1, -2]}
#update_dictionary(d, 1, -3)
#print(d)
#



def modify_list(l):
    l[:] = [i//2 for i in l if not i % 2]
    return l
    
    
    
b=[]
#a = [[0 for j in range(m)] for i in range(n)]
#b = [[9, 5, 3], [0, 7, -1], [-5, 2, 9], [end]]
c = ['end']
while c not in b:
    a=[i for i in input().split()]
    b.append(a)
b.remove(c)
h=int(len(b))
w=int(len(b[0]))
print("111", h , "222",w ,"333")

for i in range(h):
    for j in range(w):
        sum=int(b[i][j-1])+int(b[i-1][j])+int(b[i][j-h+1])+int(b[i-w+1][j])
        print(sum, end=' ')
    print ()

# a = [[0 for j in range(m)] for i in range(n)]

# for i in range(n):
#     for j in range(m):
#         print(a[i][j], end='')
#     print()



#print(*b) печатает элементы списка через пробел. Звездочка говорит принту печатать "начинку" списка, без "обрамления".


rows_count = 1
end_string = ['end']
while end_string not in b:
    a = [x for x in input().split()]
    rows_count += 1
    b.append(a)
b.remove(end_string)

for i in range(len(a)):
    for j in range(len(b)):
        print(b[i][j])
        
        
        
        



a=[int (i) for i in input().split()]
b=int(input())

if b in a:
    for i in range(len(a)):
        if a[i]==b:
            print (i, end=' ')
else:
    print ('Отсутствует')






a=int(input())

z=1
massive = []
for i in range(0,a):
    b = [z] * z
    massive+=b
    z+=1

f = massive[0:a]
for i in range (len(f)):
    print (f[i], end=' ')




a = [int(input())]
while sum(a) != 0:
    a.append(int(input()))
print (sum(i**2 for i in a))


a = [int(i) for i in input().split()]
#a.sort()

for i in set(a):
    if a.count(i) > 1:
        print (i, end=' ')


print (a[1] + a[-1], end=' ')
for i in range(len(a)-2):
    print (a[i] + a[i+2], end=' ')
print (a[0] + a[-2])

# print(sum([int(i) for i in input().split()]))


#string= str(input())
string = 'aaaabbcaa'
b = string + '0'
massive = []
symbol_number = 0

while symbol_number < len(string):
  if string[symbol_number] == b[symbol_number+1]: 
    massive.append(string[symbol_number])
  else:
    massive.append(string[symbol_number])
    print (str(string[symbol_number])+str(len(massive)), end='')
    massive.clear()
  symbol_number+=1
  
  

i = 0
counter = 0
while i < len(s):
    if s[i] == s[i - 1]:
        print(counter)
    i += 1

from test_helper import check_samples

if __name__ == '__main__':
    check_samples(samples=[["aaaabbcaa", "a4b2c1a2"], ["abc", "a1b1c1"]])

a = int(input())
b = int(input())
result=1
while  (result % a)!= 0  or (result % b) != 0:
    result+=1
print (result)



print('some')
subprocess.call(["ls", "-al"])

a = int(input())
b = int(input())
c = int(input())
p = (a + b + c) / 2
s = (p * (p - a) * (p - b) * (p - c)) ** 0.5
print(s)

a = int(input())
if ((-15) < a <= 12) or (14 < a < 17) or (a >= 19):
    print('True')
else:
    print('False')


sum=0
a = int(input())
b = int(input())
while a < b:
    sum += a
    a = a + 1

print(sum)



# смешанный вывод чисел и строк
platform = first_number + second_number
print('Платформа №' + str(platform))
print('Платформа №', platform, sep='')

# числа
radius = 5
pi = 3.14159
square = pi * radius ** 2

# строки
s = 'Tom Marvolo Riddle'
tokens = s.split()
first_name = tokens[0]
middle_name = tokens[1]
last_name = tokens[2]
s2 = first_name + ' ' + middle_name + ' ' + last_name

# конструкция 'if'. важно ставить отступы!
if (s == s2):
    print('две строки равны')
else:
    print('так не бывает')

# списки (изменяемые последовательности)
houses = ['Ravenclaw', 'Hufflepuff', 'Gryffindor']
houses.append('Slytherin')

# цикл 'for'. отступы вновь важны!
for house in houses:
    print('Ten points to', house, end='!\n')
    
ListIn = [i for i in SrtIn]
print(ListIn)

# set (неупорядоченные коллекции)
birth_name = 'Tom Marvolo Riddle'
birth_name_letters = set(birth_name)
birth_name_lower_letters = set(birth_name.lower())
nickname_lower_letters = set('I am Lord Voldemort'.lower())
print(birth_name_lower_letters == nickname_lower_letters)

fib_numbers = set([1, 1, 2, 3, 5, 8, 13, 21])
prime_numbers = set([2, 3, 5, 7, 11, 13, 17, 19])
union = fib_numbers | prime_numbers
intersection = fib_numbers & prime_numbers
difference = fib_numbers - prime_numbers
symmetric_difference = fib_numbers ^ prime_numbers

# проверка принадлежности
print(3 in prime_numbers)
print(4 in prime_numbers)
print('ratio' in 'transfiguration')

# сортировка
houses.sort() # сортировка на месте
sorted_letters = sorted(houses[0]) # новый список

# словарь - соответствие из ключей в значения
# это как список, только индексы могут быть не только числами
dictionary = {}
dictionary['half-blood'] = 'полукровка'
dictionary['cauldron'] = 'котёл'
dictionary['potion'] = 'зелье'
print(dictionary['cauldron'])




#!/usr/bin/python3

import json 
import subprocess
from os import getenv

for ip_address in [e.strip() for e in getenv('IP_ADDRESS_LIST').strip().split("\n") if e]:
    print("Adding {ip_address}...".format(ip_address=ip_address))
    proc = subprocess.run(
        "curl -u {user}:{password} -s -H 'Content-type: application/json' "
        "{nagiosql_url}/api/host -X POST -d '{json}'".format(
            user=getenv('NAGIOSQL_USER'),
            password=getenv('NAGIOSQL_PASSWORD'),
            nagiosql_url=getenv('NAGIOSQL_URL'),
            json=json.dumps({
                "hostname": [{
                    "name": ip_address,
                    "ip": ip_address,
                    "template": getenv('HOST_TEMPLATE')
                }]
            })
        ),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        check=True
    )    
    print("curl exit code is {exit_code}".format(exit_code=proc.returncode))




#!/usr/bin/python3
import codecs
from os import getenv

lines = ["[all]"]
for ip_address in [e.strip() for e in getenv('IP_ADDRESS_LIST').strip().split("\n") if e]:
    lines.append("{ip_address} ansible_ssh_pass='{password}' ansible_ssh_user='{user}'".format(
        ip_address=ip_address,
        user=getenv('SSH_USERNAME'),
        password=getenv('SSH_PASSWORD')
    ))   

with codecs.open("snmp_install_inventory.ini", "w", "utf-8") as file:
    file.write("\n".join(lines))


 --- install jre





    def install_jre():
        # check for JRE
        if len(glob.glob('/opt/jre*/java/jre/bin/java')) > 0:
            return

        print "\n[+] Installing JRE"
        cmd='cd %s/JRE* ; ./jreadmin' % PKGDIR
        os.system(cmd)

        # check if installation is successful
        if len(glob.glob('/opt/jre*/java/jre/bin/java')) == 0:
            print "Failed to install JRE"
            print "\nDone!"
            sys.exit(100) # general error


    #!/usr/bin/python

    import os

    lines = []
    for ip in range(1,256):
        lines.append("172.28.0.%s ansible_ssh_pass='pass' ansible_ssh_user=root" % ip)

    with open("docker-containers.yml", "a") as dst_tests_file:
        dst_tests_file.write("\n".join(lines)+"\n")



    def create_ftp_user():
        username = "foobar"
        check_user = os.system("id -u %s" % username) >> 8
        if check_user != 0:
            print "Adding %s user to system" % username
            os.system("useradd %s" % username)

        os.environ["HOME"] = "/path/"
        p = pexpect.spawn('/path/to/file')
        p.logfile = sys.stdout
        p.timeout = 30
        confirm(p, 'Would you like to set up FTP users')
        print_value(p, 'Enter FTP', username)
        confirm(p, 'User foobar exists')
        print_value(p, 'password:', 'test1')
        print_value(p, 'password:', 'test1')
        print_value(p, 'add another user', 'n')
        confirm(p, 'enable Mutual Authentication?')
        p.expect('Done')
        p.close()
    #!/usr/bin/python

    def custom_filtter(n):
        #  print '%03d' % n
        return  '%03d' % n
    print (custom_filtter(4))
    #print (a)






     #!/usr/bin/env python3
     # TODO
     # string consist of 1 symbol
     # separate counting of high symbols


     input_string = 'aaaabbcaa'
     count=0
     t=''

     for letter in range (len(input_string)-1):
         if input_string[letter]==input_string[letter+1]:
             count+=1
         else:
             t=t+input_string[letter]+str(count)

     print (t, letter, count)


     #if (time.strftime("%H:%M")) == "09:30":
     #    os.system("morning")
     #    print("messgage")
     #
     #if (time.strftime("%w")) == "1":
     #
     # print (s[3:6])
     # print (s[:6])
     # print (s[3:])
     # print (s[::-1])
     # print (s[-3:])
     # print (s[:-6])
     # print (s[-1:-10:-2])
     #

     #s='foobar text string'
     #print (s.lower().count('a')


     #print (s.upper())
     #print (s.upper().count('gt'.upper()))
     #print (s.upper().count('gt'.upper()))
     #print (s.upper().count('gt'.upper()))
     #print (len(s))
     # slice
     #dna[1:-1:2]



     #a=int(input())
     #b=int(input())
     #s=0
     #counter=0
     #for i in range (a,b+1):
     #    if i % 3 == 0 :
     #        s+=i
     #        counter+=1
     #        print (i)
     #b=s/i
     #print (s)
     #print (counter)
     #print (s/counter)
     #
     #a,b=(int(i) for i in input().split())


     #c=int(input())
     #d=int(input())
     #for y in range (c,d+1):
     #    print('\t'+str(y),end='')
     #print(end='\n')

     #for y in range (a,b+1):
     #    print(str(y)+'\t',end='')
     #    for x in range (c,d+1):
     #        print(str(x*y),end='\t')
     #    print(end='\n')


     #print(str(a*b), "\x1b[32m", end='\t')

     #print (hello)




     # color
     #print(str(i*j), "\x1b[32m", end='\t')
     #while true :
     #    i=int(nput())
     #    if i >=100:
     #        break
     #    if i < 10:
     #        continue
     #    else:
     #        print (i)
     #

     #i = 0
     #s = 0
     #while i < 10:
     #    i = i + 1
     #    s = s + i
     #    if s > 15:
     #        break
     #    i = i + 1
     #print (i)
     #


     # palindrom
     # s = input()
     # r = s[::-1]
     # if s == r:

     #s = input()
     #i = 0
     #j = len(s) - 1
     #is_palindrom = True
     #while i < j:
     #    if s[i] != s[j]:
     #        is_palindrom = False
     #    i += 1
     #    j -= 1
     #if is_palindrom:
     #    print('YES')
     #else:
     #    print('NO')
     #




     # a=str(input())
     # if (int(a[0])+int(a[1])+int(a[2]))==(int(a[3])+int(a[4])+int(a[5])):
     #  print ("good")
     #else:
     #  print ('not')
     #
     #
     #n = list(map(int, list(input())))
     #print('good' if sum(n[:3]) == sum(n[3:]) else 'not')
     #
     #a,b,c,d,e,f=(int(n) for n in input())
     #print(('not','good')[a+b+c == d+e+f])
     #



     # print (max (arr))
     # print (min (arr))
     # print ( a+b+c - max(arr) - min(arr))
     # print ('sad')
     # #print(str(max_int)+'\n'+str(min_int)+'\n'+str(last_int))
     # #print(a, b, c, sep = ':')


     # a = int (input())
     # arr = [a, b, c]
     # print (max (arr))
     # print (min (arr))
     # print ( a+b+c - max(arr) - min(arr))


     #numbers = list(range(3, 8))
     #print(numbers)
     #print(range(20) == range(0, 20))


     # str = "Hello world!"
     # print(str[6])
     # w



     #figure = input()
     #if c == 'triangle':
     #    a = float (input())
     #    b = float (input())
     #    c = float (input())
     #    p = (a+b+c)/2
     #    print ( sqrt(p*(p-a)*(p-b)*(p-c)))
     #
     #if c == 'square':
     #    a = float (input())
     #    b = float (input())
     #    print ( a*b)
     #
     #if c == 'circle':
     #    a = float (input())
     #    print ( a*a*3.14)
     #





     #
     #if c == '+':
     #    print ( a + b )
     #if c == "-":
     #    print ( a - b )
     #if c == "*":
     #    print ( a * b )
     #if c == "/":
     #    if b==0:
     #      print ("division by 0!")
     #    else:
     #      print ( a / b )
     #if c == "mod":
     #    if b==0:
     #      print ("division by 0!")
     #    else:
     #      print ( a % b )
     #if c == "pow":
     #    print ( a **  b )
     #if c == "div":
     #    if b==0:
     #      print ("division by 0!")
     #    else:
     #      print ( a // b )



     #if a % 400 == 0:
     #else
     #  if a % 4 == 0  a % 100 != 0 :
     #  else
     #



     #if h < a:
     #  print ("Nedo")
     #else
     #    if h < b :
     #      print ("norm")
     #    else
     #      print ("over")






     #x = int (input())
     #y = int (input())
     #z = int (input())
     #end = x + y * 60 + z
     #print (end // 60 )
     #print (end  % 60 )
     #



     # file = open("/path/to/file", "r")
     # output = file.read()
     # print(output)
     # file.close()
     #
     #
     # msg = "example message"
     # file = open("/home/mel/downloads/new.txt", "r")
     # amount_written = file.write(msg)
     # file.close()
     #
     # file = open("/path/to/file", "r")
     # output = file.read()
     # print (amount_written)
     # print(output)
     # file.close()
     #
     #
     # import os
     # import time
     #
     # start_time = time.time()
     #
     # print('start')
     # nameA = 'AA'
     # var2 = '111111111'
     # ip6 = '2001:abda:0:a101::29'
     # counter = 1
     # while counter <= 63:
     #      nameA = nameA[:2] + str(counter)
     #      print(nameA)
     #      var2 = var2[:7] + str(counter)
     #      print(var2)
     #      ip6 = ip6[:17] + str(counter)
     #      print(ip6)
     #      counter += 1
     #
     #
     # finish_time = time.time() - start_time
     # print(finish_time // 60, "minutes")
     #
     #
     #
     #
     # import os
     # lines = []
     # for ip in range(1, 256):
     #     lines.append("172.16.0.%s ansible_ssh_pass='password' ansible_ssh_user=root" % ip)
     #
     # with open("example.yml", "a") as dst_tests_file:
     #     dst_tests_file.write("\n".join(lines) + "\n")
     #
     #  check if dir exist
     #     if not os.path.exists('/home/output'):
     #      os.makedirs('/home/output')
     #
     #
     #
     #   remote command execution over ssh   https://habr.com/post/150047/
     #  import paramiko
     #
     #  host = '192.168.0.8'
     #  user = 'login'
     #  secret = 'password'
     #  port = 22
     #
     #  client = paramiko.SSHClient()
     #  client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
     #  client.connect(hostname=host, username=user, password=secret, port=port)
     #  stdin, stdout, stderr = client.exec_command('ls -l')
     #  data = stdout.read() + stderr.read()
     #  client.close()
     #
     #  FizzBuzz test
     #for i in range(1,101):
     #  x = lambda z: False if i % z else True
     #  if(x(3) and x(5)): print "FizzBuzz"
     #  if(x(3)): print "Fizz"
     #  elif(x(5)): print "Buzz"
     #  else: print i



    #!/usr/bin/env python3

    import jenkins
    import sys

    #print (sys.__file__)
    print (jenkins.__file__)
    #server = jenkins.Jenkins('sys.argv[0]', username='sys.argv[1]', password='sys.argv[2]')
    #server = jenkins.Jenkins('http://ip_address:port/', username='1', password='1')
    #user = server.get_whoami()
    #version = server.get_version()
    #print('Hello %s from Jenkins %s' % (user['fullName'], version))
    #print ("Привет, {}!".format (sys.argv[0] ) )


  password generator
  from random import choice
  from string import ascii_uppercase
  print(''.join(choice(ascii_uppercase) for i in range(12)))





    #!/usr/bin/python3

    import pexpect
    import sys


    def set_root_password(p):   # создаем функцию
        p.expect('password: ')   # ждем пароль
        p.sendline('root')    # передача root

    if __name__ == "__main__":   # объявление функции main

        args = sys.argv[1:]    #берем все аргументы

        if not args:     #аргумента нет
            print ("Укажите аргумент")
            sys.exit(-1)       #выход из скрипта

        elif (sys.argv[1] == "ssh_localhost"): # аргумент равен ssh_localhost

            p = pexpect.spawn ('ssh_localhost') #вызов ssh_localhost
            p.logfile = sys.stdout #стандартный вывод
            print(p)
            set_root_password(p) #вызов функции
            p.expect('Готово!')

        else:
            print (sys.argv[1] + " Невалидный для установки")
            sys.exit(-1)          #выход из скрипта

        sys.exit(0)









# смешанный вывод чисел и строк
platform = first_number + second_number
print('Платформа №' + str(platform))
print('Платформа №', platform, sep='')

# числа
radius = 5
pi = 3.14159
square = pi * radius ** 2

# строки
s = 'Tom Marvolo Riddle'
tokens = s.split()
first_name = tokens[0]
middle_name = tokens[1]
last_name = tokens[2]
s2 = first_name + ' ' + middle_name + ' ' + last_name

# конструкция 'if'. важно ставить отступы!
if (s == s2):
        print('две строки равны')
    else:
            print('так не бывает')

# списки (изменяемые последовательности)
houses = ['Ravenclaw', 'Hufflepuff', 'Gryffindor']
houses.append('Slytherin')

# цикл 'for'. отступы вновь важны!
for house in houses:
        print('Ten points to', house, end='!\n')

# set (неупорядоченные коллекции)
birth_name = 'Tom Marvolo Riddle'
birth_name_letters = set(birth_name)
birth_name_lower_letters = set(birth_name.lower())
nickname_lower_letters = set('I am Lord Voldemort'.lower())
print(birth_name_lower_letters == nickname_lower_letters)

fib_numbers = set([1, 1, 2, 3, 5, 8, 13, 21])
prime_numbers = set([2, 3, 5, 7, 11, 13, 17, 19])
union = fib_numbers | prime_numbers
intersection = fib_numbers & prime_numbers
difference = fib_numbers - prime_numbers
symmetric_difference = fib_numbers ^ prime_numbers

# проверка принадлежности
print(3 in prime_numbers)
print(4 in prime_numbers)
print('ratio' in 'transfiguration')

# сортировка
houses.sort() # сортировка на месте
sorted_letters = sorted(houses[0]) # новый список

# словарь - соответствие из ключей в значения
# это как список, только индексы могут быть не только числами
dictionary = {}
dictionary['half-blood'] = 'полукровка'
dictionary['cauldron'] = 'котёл'
dictionary['potion'] = 'зелье'
print(dictionary['cauldron'])






 aws modify security groups
security_group.revoke_ingress(IpProtocol="tcp", CidrIp="0.0.0.0/0", FromPort=3306, ToPort=3306)
security_group.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=3306,ToPort=3306)



!/usr/bin/python3

import json 
import subprocess
from os import getenv

for ip_address in [e.strip() for e in getenv('IP_ADDRESS_LIST').strip().split("\n") if e]:
    print("Adding {ip_address}...".format(ip_address=ip_address))
    proc = subprocess.run(
        "curl -u {user}:{password} -s -H 'Content-type: application/json' "
        "{nagiosql_url}/api/host -X POST -d '{json}'".format(
            user=getenv('NAGIOSQL_USER'),
            password=getenv('NAGIOSQL_PASSWORD'),
            nagiosql_url=getenv('NAGIOSQL_URL'),
            json=json.dumps({
                "hostname": [{
                    "name": ip_address,
                    "ip": ip_address,
                    "template": getenv('HOST_TEMPLATE')
                }]
            })
        ),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        check=True
    )    
    print("curl exit code is {exit_code}".format(exit_code=proc.returncode))




#!/usr/bin/python3
import codecs
from os import getenv

lines = ["[all]"]
for ip_address in [e.strip() for e in getenv('IP_ADDRESS_LIST').strip().split("\n") if e]:
    lines.append("{ip_address} ansible_ssh_pass='{password}' ansible_ssh_user='{user}'".format(
        ip_address=ip_address,
        user=getenv('SSH_USERNAME'),
        password=getenv('SSH_PASSWORD')
    ))   

with codecs.open("snmp_install_inventory.ini", "w", "utf-8") as file:
    file.write("\n".join(lines))



"""
