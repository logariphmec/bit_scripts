#!/usr/bin/python

import sys
import pexpect
import glob

if len(sys.argv) == 1:
    sys.exit("ERROR: please specify backup file")
else:
    path = sys.argv[1]

def inject_value(p, message, value):
    result = p.expect(message)
    if result == 0:
        p.sendline(value)

def restore_db():
    #script_path = glob.glob('/path/?*.0.0/restore -i' + path)[0]
    #p = pexpect.spawn(script_path)
    #script_path = glob.glob('/path/?*.0.0/restore')[0]
    #p = pexpect.spawn(script_path -i + path)
    script_path = glob.glob('/*.sh')[0]
    p = pexpect.spawn(script_path -i + path)
    p.logfile = sys.stdout
    inject_value(p, 'DB Username', 'user')
    inject_value(p, 'Password', 'user')
    inject_value(p, 'Please reenter password', 'user')
    inject_value(p, 'Are you sure to create a new DB', 'y')
    inject_value(p, 'Do you want to backup the existing DB', 'n')
    p.expect('Done')
    p.expect('Redirecting to /bin/systemctl stop mysqld.service')

if __name__ == "__main__":
    restore_db()
