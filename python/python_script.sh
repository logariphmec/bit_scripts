#!/usr/bin/python

import pexpect
import sys


def set_root_password(p): #создаем функцию 
    p.expect('password: ') #ждем пароль
    p.sendline('root') #передача root

if __name__ == "__main__": #объявление функции main

    args = sys.argv[1:]    #берем все аргументы 

    if not args: 	   #аргумента нет
        print "Укажите аргумент"
        sys.exit(-1)       #выход из скрипта
    
    elif (sys.argv[1] == "ssh_localhost"): # аргумент равен ssh_localhost

        p = pexpect.spawn ('ssh_localhost') #вызов ssh_localhost
        p.logfile = sys.stdout #стандартный вывод
        print(p)
        set_root_password(p) #вызов функции 
        p.expect('Готово!') 
    
    else:
        print sys.argv[1] + " Невалидный для установки"
        sys.exit(-1)          #выход из скрипта

  
    sys.exit(0) 


