resource "aws_launch_configuration" "example" {
#resource "aws_instance" "example" {
  #image_id        = "ami-06761466b07a8dbc0" #  debian on eu-central-1
  image_id = "${var.ami}"
  #  image_id        = "ami-40d28157"
  #  ami = "ami-06761466b07a8dbc0" #  debian on eu-central-1
  instance_type   = "${var.instance_type}"
  security_groups = ["${aws_security_group.instance.id}"]
  user_data       = "${data.template_file.user_data.rendered}"
#  user_data = <<-EOF
#              #!/bin/bash
#              sudo apt -y install busybox
#              echo "Hello, World1h" > index.html
#              nohup busybox httpd -f -p "${var.server_port}" &
#              EOF
 # user_data = "${element(concat(data.template_file.user_data.*.rendered,
 #                               data.template_file.user_data_new.*.rendered),
 #                               0)}"
  key_name = "${var.key_name}"
#     provisioner "local-exec" {
#     command = "echo ${aws_instance.web.private_ip} >> private_ips.txt"
#     command = "ansible-playbook -u admin -i ‘${aws_instance.web.public_dns},’ main.yml"  # check quotes
#     }

  lifecycle {
    create_before_destroy = true
  }
}

data "template_file" "user_data" {
  template = "${file("${path.module}/user-data.sh")}"

  vars {
    server_port = "${var.server_port}"
    db_address  = "${data.terraform_remote_state.db.address}"
    db_port     = "${data.terraform_remote_state.db.port}"
  }
}

resource "aws_autoscaling_group" "example" {
  launch_configuration = "${aws_launch_configuration.example.id}"
  availability_zones   = ["${data.aws_availability_zones.all.names}"]
  load_balancers       = ["${aws_elb.example.name}"]
  health_check_type    = "ELB"

  min_size = "${var.min_size}"
  max_size = "${var.max_size}"

  tag {
    key                 = "Name"
    value               = "${var.cluster_name}"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_schedule" "scale_out_at_morning" {
  count = "${var.enable_autoscaling}"
  max_size = 4
  min_size = 3
  recurrence = "0 9 * * *"
  autoscaling_group_name = "${aws_autoscaling_group.example.name}"
  scheduled_action_name = "scale_out_at morning"
}

resource "aws_autoscaling_schedule" "scale_in_at_evening" {
  count = "${var.enable_autoscaling}"
  max_size = 2
  min_size = 2
  recurrence = "0 17 * * *"
  autoscaling_group_name = "${aws_autoscaling_group.example.name}"
  scheduled_action_name = "scale_in_at_evening"
}

resource "aws_security_group" "instance" {
  name = "${var.cluster_name}-instance"

#  ingress {
#    #from_port   = "${var.server_port}"
#    from_port   = 0
#    to_port   = 65335
#    protocol    = "-1"
#    cidr_blocks = ["${var.MYIP}/32"]
#    #cidr_blocks = ["0.0.0.0/0"]
#  }
#  egress {
#    from_port   = 0
#    to_port     = 65335
#    protocol    = "-1"
#    cidr_blocks = ["0.0.0.0/0"]
#    #cidr_blocks = ["${var.MYIP}/32"]
#  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "allow_server_http_inbound" {
  type              = "ingress"
  security_group_id = "${aws_security_group.instance.id}"

  from_port   = "${var.server_port}"
  to_port     = "${var.server_port}"
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

data "aws_availability_zones" "all" {}

resource "aws_elb" "example" {
  name               = "${var.cluster_name}"
  availability_zones = ["${data.aws_availability_zones.all.names}"]
  security_groups    = ["${aws_security_group.elb.id}"]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = "${var.server_port}"
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    target              = "HTTP:${var.server_port}/"
  }
}

resource "aws_security_group" "elb" {
  name = "${var.cluster_name}-elb"
}

resource "aws_security_group_rule" "allow_http_inbound" {
  type              = "ingress"
  security_group_id = "${aws_security_group.elb.id}"

  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_all_outbound" {
  type              = "egress"
  security_group_id = "${aws_security_group.elb.id}"

  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

data "terraform_remote_state" "db" {
  backend = "s3"

  config {
    #bucket = "${var.db_remote_state_bucket}"
    bucket = "${var.DB_REMOTE_STATE_BUCKET}"
    key    = "${var.db_remote_state_key}"
    #region = "eu-central-1"    # perhaps don't need for s3
  }
}

#resource "aws_cloudwatch_metric_alarm" "high_cpu_utilization" {
#  alarm_name = "${var.cluster_name}-high-cpu-utilization"
#  comparison_operator = "GreaterThanThreshold"
#  evaluation_periods = 1
#  metric_name = "CPUUtilization"
#  namespace = "AWS/EC2"
#  period = 300
#  threshold = 90
#  unit = "Percent"
#  dimensions = {
#    AutoScalingGroupName = "${aws_autoscaling_group.example.name}"
#  }
#}
