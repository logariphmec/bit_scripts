resource "digitalocean_droplet" "web1" {
  image = "ubuntu-16-04-x64"
  name = "web1"
  region = "nyc3"
  size = "512mb"
  private_networking = true
#  user_data = "${file("config/install_nginx.sh")}"
  ssh_keys = [
    "${var.SSH_FINGERPRINT}"
  ]
  connection {
    user = "root"
    type = "ssh"
    private_key = "${file(var.PRIVATE_KEY)}"
    timeout = "2m"
  }
#  provisioner "remote-exec" {
#    inline = [
#      "apt update",
#      "apt install -y nginx"
#    ]
#  }
#
}

