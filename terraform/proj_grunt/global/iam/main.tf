provider "aws" {
  region     = "eu-central-1"
}

#data "aws_iam_policy_document" "ec2_read_only" {
#  "statement" {
#    effect = "Allow"
#    actions = ["ec2:Describe*"]
#    resources = ["*"]
#  }
#}
#
#resource "aws_iam_policy" "ec2_read_only" {
#  name = "ec2-read-only"
#  policy = "${data.aws_iam_policy_document.ec2_read_only.json}"
#}
#resource "aws_iam_user" "example" {
#  name = "example"
#}
#
#resource "aws_iam_policy_attachment" "ec2_access" {
#  count = "${length(var.user_names)}"
#  user = "${element(aws_iam_user.example.*.name, count.index)}"
#  policy_arn = "${aws_iam_policy.ec2_read_only.arn}"
#}
#
#
#variable "user_names" {
#  type = "list"
#  default = ["neo", "trinity", "morpheus"]
#}
