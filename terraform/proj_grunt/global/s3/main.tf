provider "aws" {
  region     = "eu-central-1"
}

resource "aws_s3_bucket" "terraform_state-storage-s3" {
  bucket = "${var.DB_REMOTE_STATE_BUCKET}"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  tags {
    Name = "S3 Remote Terraform State Store"
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock-proj_grunt" {
  name = "terraform-state-lock-dynamo-proj_grunt"
  hash_key = "LockID"
  read_capacity = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "DynamoDB Terraform State Lock Table"
  }
}
