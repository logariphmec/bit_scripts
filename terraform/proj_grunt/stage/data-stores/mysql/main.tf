provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_db_instance" "example" {
  engine              = "mysql"
  allocated_storage   = 10
  instance_class      = "db.t2.micro"
  name                = "example_database_stage"
  username            = "admin"
  password            = "${var.AWS_MYSQL_PASSWORD}"
  skip_final_snapshot = true
}
