terraform {
  backend "s3" {
    encrypt = true
    bucket = "foobar"
    dynamodb_table = "terraform-state-lock-dynamo-proj_grunt"
    key = "proj_grunt/stage/data-stores/mysql/statefile.tfstate"
    region = "eu-central-1"
  }
}
