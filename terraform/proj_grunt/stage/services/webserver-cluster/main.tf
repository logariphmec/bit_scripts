#terraform {
#  required_version = ">= 0.8, < 0.9"
#}

provider "aws" {
  #provider.aws: version = "~> 1.46" {
  #  access_key = "${var.aws_access_key}"
  #  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

module "webserver_cluster" {
  source = "../../../../modules/services/webserver-cluster"
  #source = "git@github.com:brikis98/terraform-up-and-running-code.git//code/terraform/04-terraform-module/module-example/modules/services/webserver-cluster?ref=v0.0.2"
  ami = "${data.aws_ami.debian.id}"
  region = "${var.aws_region}"

  enable_autoscaling = "${var.enable_autoscaling}"
  cluster_name           = "${var.cluster_name}"
  #db_remote_state_bucket = "${var.db_remote_state_bucket}"
  db_remote_state_bucket = "${var.DB_REMOTE_STATE_BUCKET}"
  db_remote_state_key    = "${var.db_remote_state_key}"
  key_name = "${var.AWS_KEY_NAME}"

  instance_type = "t2.micro"
  min_size      = "${var.asg_min_size}"
  max_size      = "${var.asg_max_size}"
}

data "aws_ami" "debian" {
  most_recent = true

  filter {
    name = "name"
    values = [ "debian-jessie-*"]
#    values = [ "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = [ "hvm"]
  }

  filter {
    name = "architecture"
    values = [ "x86_64"]
  }

  filter {
    name = "root-device-type"
    values = [ "ebs"]
  }
  #owners = ["099720109477"] # Canonical
  #owners = ["379101102735"] # Debian
}

#resource "aws_instance" "example" {
#### count = "${format("%.ls", var.instance_type) == "t" ? 1 : 0}"
##### count = "${1 - var.some_variable_enable}"
##  count = 3
##  name = "neo.${count.index}"
###  count = "${length(var.server_names)}"
###  name = "${element(var.server_names, count.index)}"
#  ami = "ami-06761466b07a8dbc0" #  debian on eu-central-1
#  instance_type   = "${var.instance_type}"
#  security_groups = ["${aws_security_group.instance.id}"]
#  key_name = "${var.key_name}"
#
#  lifecycle {
#    create_before_destroy = true
#  }
#}
