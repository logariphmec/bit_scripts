terraform {
  backend "s3" {
    encrypt = true
    bucket = "terraform-statefile51251256"
    dynamodb_table = "terraform-state-lock-dynamo-proj_grunt"
    key = "proj_grunt/stage/services/webserver-cluster/statefile.tfstate"
    region = "eu-central-1"
  }
}
